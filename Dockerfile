FROM node
WORKDIR /src
COPY . .
RUN npm install
EXPOSE 4300
CMD node server.js