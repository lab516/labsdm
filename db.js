const mysql=require('mysql2')
const pool=mysql.createPool({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '1234',
    waitForConnections: true,
    database: 'my_db'
})

module.exports ={
    pool,
}